[![pipeline status](https://gitlab.com/octomy/base-batch/badges/production/pipeline.svg)](https://gitlab.com/octomy/base-batch/-/commits/production)

# About base-batch

<img src="https://gitlab.com/octomy/base-batch/-/raw/production/design/logo-1024.png" width="20%"/>

Docker image that serves as a base for all batch images in octomy

- base-batch is [available on gitlab](https://gitlab.com/octomy/base-batch).
- base-batch is [available in gitlab container registry](https://gitlab.com/octomy/base-batch/container_registry).

```shell
# Clone git repository
git clone git@gitlab.com:octomy/base-batch.git

```

```shell
# Pull from container registry
docker pull registry.gitlab.com/octomy/base-batch

```
