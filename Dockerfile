# syntax = docker/dockerfile:1.0-experimental
FROM registry.gitlab.com/octomy/base-py:1.0.0

ENV PATH="$VIRTUAL_ENV/bin:$HOME/.local/bin:${PATH}"

COPY --chown=8888:8888 fk_batch_filters/ ./fk_batch_filters
COPY --chown=8888:8888 README.md VERSION ./
# Data driven configuration defaults (will be overridden by local_config.json and environment variables in that order)
COPY --chown=8888:8888 config.json ./
COPY --chown=8888:8888 requirements/ ./requirements
COPY --chown=8888:8888 setup.cfg setup.py ./

RUN ls -halt
RUN pip install -e ./
RUN pip freeze

CMD ["octomy-batch"]


